package si.uni_lj.fri.pbd.miniapp1

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONObject

class ListFragment : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<*>? = null
    private var stringList = ArrayList<MemoModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        recyclerView = view.findViewById(R.id.recycler_view)
        layoutManager = LinearLayoutManager(view.context)
        recyclerView?.layoutManager = layoutManager
        val sharedPref = (activity as MainActivity).getSharedPreferences("Memos",
            Context.MODE_PRIVATE).all as HashMap<String, String>
        sharedPref.forEach { (_, v) ->
            val jObj = JSONObject(v)
            val loadedMemo = MemoModel(
                jObj["time"] as String,
                jObj["title"] as String,
                jObj["description"] as String,
                null
            )
            loadedMemo.stringToBitmap(jObj["image"] as String)
            stringList.add(loadedMemo)
        }
        adapter = RecyclerAdapter(parentFragmentManager, stringList)
        recyclerView?.adapter = adapter
        val btnNewFragment = view.findViewById<FloatingActionButton>(R.id.fab)
        btnNewFragment.setOnClickListener { v ->
            val switchFragment = parentFragmentManager.beginTransaction()
            switchFragment.replace(R.id.fragment_container, NewFragment())
            switchFragment.addToBackStack(null);
            stringList.clear()
            switchFragment.commit()

         }

        return view
    }
    override fun onResume() {
        super.onResume()
        adapter?.notifyDataSetChanged()

    }
}