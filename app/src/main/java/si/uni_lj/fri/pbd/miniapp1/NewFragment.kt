package si.uni_lj.fri.pbd.miniapp1

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*


class NewFragment : Fragment() {
    var newTime: String? = null
    var newImage: Bitmap? = null
    private var ifImage: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_new, container, false)
        val btnSaveMemo = view.findViewById<Button>(R.id.save_memo)
        btnSaveMemo.setOnClickListener { v ->
            val ifTitle = view.findViewById(R.id.new_title) as EditText
            val ifDescr = view.findViewById(R.id.new_description) as EditText
            if (
                ifTitle.length() > 0  && ifDescr.length() > 0 && ifImage
            )
            {
                saveMemo()
                val switchFragment = parentFragmentManager.beginTransaction()
                switchFragment.replace(R.id.fragment_container, ListFragment())
                switchFragment.commit()
            } else {
                Snackbar.make(view, "Missing text and/or image",
                    Snackbar.LENGTH_LONG).show()
            }
        }

        val btnSavePhoto = view.findViewById<Button>(R.id.take_image)
        btnSavePhoto.setOnClickListener { v ->
            savePhoto()
        }

        return view
    }
    private fun saveMemo() {
        val newTitle = view?.findViewById<EditText>(R.id.new_title)?.text.toString()
        val newDescription = view?.findViewById<EditText>(R.id.new_description)?.text.toString()
        ///val newID = (activity as MainActivity).listSize
        (activity as MainActivity).incrementListSize(1)
        var newMemo = MemoModel(newTime,newTitle,newDescription,newImage)
        val textAdd = newMemo.MemoToJson()
        val sharedPrefs = (activity as MainActivity).getSharedPreferences("Memos",Context.MODE_PRIVATE)
        with(sharedPrefs.edit()) {
            putString(newTime,textAdd)
            commit()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK) {
            newTime = SimpleDateFormat("HH:mm:ss MM/dd/yyyy", Locale.GERMANY).format(Date())
            val newImageView = view?.findViewById<ImageView>(R.id.new_image)
            newImage = data?.extras?.get("data") as Bitmap
            newImageView?.setImageBitmap(newImage)
            ifImage = true
        }

    }
    private fun savePhoto() {
        val takePicIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePicIntent, 1)
        } catch (e: ActivityNotFoundException) {

        }
    }
}



