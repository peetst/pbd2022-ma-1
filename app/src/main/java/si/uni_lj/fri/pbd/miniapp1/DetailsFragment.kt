package si.uni_lj.fri.pbd.miniapp1

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONObject
import java.io.ByteArrayOutputStream

class DetailsFragment(
   val timestamp: String?
    ) : Fragment(R.layout.fragment_details) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        val sharedPrefs = (activity as MainActivity).getSharedPreferences("Memos",
            Context.MODE_PRIVATE)
        val stringPref = sharedPrefs.getString(timestamp,"Error")
        val jObj = JSONObject(stringPref.toString())
        val loadedMemo = MemoModel(
            jObj["time"] as String,
            jObj["title"] as String,
            jObj["description"] as String,
            null
        )
        loadedMemo.stringToBitmap(jObj["image"] as String)
        view.findViewById<TextView>(R.id.detail_timestamp).text = loadedMemo.memoGetTime()
        view.findViewById<TextView>(R.id.detail_title).text = loadedMemo.memoGetTitle()
        view.findViewById<TextView>(R.id.detail_description).text = loadedMemo.memoGetDescription()
        view.findViewById<ImageView>(R.id.detail_image).setImageBitmap(loadedMemo.memoGetImage())

        val btnDelete = view.findViewById<Button>(R.id.detail_delete)
        btnDelete.setOnClickListener {
            with(sharedPrefs.edit()){
                remove(timestamp)
                apply()
            }
            val switchFragment = parentFragmentManager.beginTransaction()
            switchFragment.replace(R.id.fragment_container, ListFragment())
            switchFragment.commit()

        }
        val btnShare= view.findViewById<Button>(R.id.detail_share)
        btnShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.data = Uri.parse("mailto:")
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, loadedMemo.memoGetTitle())
            intent.putExtra(Intent.EXTRA_TEXT, loadedMemo.memoGetTime() +
                    "\n" + loadedMemo.memoGetDescription())
            val imageBitmap = loadedMemo.memoGetImage()
            val byteStream = ByteArrayOutputStream()
            imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, byteStream)
            val path = MediaStore.Images.Media.insertImage(context?.contentResolver,
            imageBitmap,"temp", null)
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path))
            startActivity(Intent.createChooser(intent, "Select app"))
        }
        return view
    }

}