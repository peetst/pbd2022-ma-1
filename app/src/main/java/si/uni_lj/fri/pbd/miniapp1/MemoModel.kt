package si.uni_lj.fri.pbd.miniapp1

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import android.util.Base64

data class MemoModel(
    val time: String?,
    val title: String,
    val description: String,
    var image: Bitmap?
) {
    fun MemoToJson(): String {
        val JSONReturn = JSONObject()
        JSONReturn.put("time",this.time)
        JSONReturn.put("title",this.title)
        JSONReturn.put("description",this.description)
        val byteOut = ByteArrayOutputStream()
        this.image?.compress(Bitmap.CompressFormat.JPEG, 100, byteOut)
        val compressedImage = byteOut.toByteArray()
        val imageString = Base64.encodeToString(compressedImage, Base64.DEFAULT)
        JSONReturn.put("image", imageString)
        return JSONReturn.toString()
    }

    fun stringToBitmap(imgString: String) {
        val imageByte = Base64.decode(imgString, 0)
        this.image = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.size)


    }

    fun memoGetImage(): Bitmap? {
        return this.image
    }

    fun memoGetTime(): String? {
        return this.time
    }

    fun memoGetTitle(): String {
        return this.title
    }

    fun memoGetDescription(): String {
        return this.description
    }
}
