package si.uni_lj.fri.pbd.miniapp1

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.*
import android.content.SharedPreferences


class MainActivity : AppCompatActivity() {
    var listSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPref = this.getSharedPreferences("Memos", Context.MODE_PRIVATE)
        listSize = sharedPref.all.size
        //with(sharedPref.edit()) {
        //    clear()
        //    commit()
        //}
        if(savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<ListFragment>(R.id.fragment_container)
                }
        }
    }

    fun incrementListSize(n: Int) {
        this.listSize += 1
    }
}