package si.uni_lj.fri.pbd.miniapp1

import android.content.Context
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.miniapp1.RecyclerAdapter.CardViewHolder

class RecyclerAdapter(
    var parentFragmentManager: FragmentManager,
    var dataList: ArrayList<MemoModel>
) : RecyclerView.Adapter<CardViewHolder?>() {

    inner class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var listImage: ImageView = itemView.findViewById(R.id.list_image)
        var listTitle: TextView = itemView.findViewById(R.id.list_title)
        var listTimestamp: TextView = itemView.findViewById(R.id.list_timestamp)

        init {
            itemView.setOnClickListener {
                val switchFragment = parentFragmentManager.beginTransaction()
                switchFragment.replace(R.id.fragment_container,
                    DetailsFragment(listTimestamp.text.toString()))
                switchFragment.addToBackStack(null);
                dataList.clear()
                switchFragment.commit()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_memo_model,
            parent, false)
        return (CardViewHolder(view))
    }

    override fun onBindViewHolder(holder: CardViewHolder, i: Int) {
        holder.listImage.setImageBitmap(dataList[i].memoGetImage())
        holder.listTitle.text = (dataList[i].memoGetTitle())
        holder.listTimestamp.text = (dataList[i].memoGetTime())
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}